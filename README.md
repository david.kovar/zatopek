# Zátopek (zatopek)

Result tables for running and athletics

### Development

To run in development mode:

```
quasar dev -m electron
```

To enable auto completition in IntelliJ Idea, install "vue.js" plugin and in root directory run this command:

```
./script/code_completition.sh >.qcomponents.js
```

### Lint the files

```bash
yarn lint
# or
npm run lint
```

### Format the files

```bash
yarn format
# or
npm run format
```

### Build the app for production

```bash
quasar build
```
