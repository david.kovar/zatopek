/**
 * This file is used specifically for security reasons.
 * Here you can access Nodejs stuff and inject functionality into
 * the renderer thread (accessible there through the "window" object)
 *
 * WARNING!
 * If you import anything from node_modules, then make sure that the package is specified
 * in package.json > dependencies and NOT in devDependencies
 *
 * Example (injects window.myAPI.doAThing() into renderer thread):
 *
 *   import { contextBridge } from 'electron'
 *
 *   contextBridge.exposeInMainWorld('myAPI', {
 *     doAThing: () => {}
 *   })
 *
 * WARNING!
 * If accessing Node functionality (like importing @electron/remote) then in your
 * electron-main.ts you will need to set the following when you instantiate BrowserWindow:
 *
 * mainWindow = new BrowserWindow({
 *   // ...
 *   webPreferences: {
 *     // ...
 *     sandbox: false // <-- to be able to import @electron/remote in preload script
 *   }
 * }
 */

import { contextBridge, ipcRenderer } from 'electron'

contextBridge.exposeInMainWorld('ipcMain', {
  //Render (Vue) to main (Electron)
  send: (channel: string, data: never) => {
    console.log(`Message from renderer ${channel}: ${JSON.stringify(data)}`);
    const validChannels = [   // <-- Array of all ipcRenderer Channels used in the client
      'person-filter-data', 'person-by-starting-number', 'person-save', 'person-delete',
      'category-get-all-data', 'category-get-all-extra', 'category-save', 'category-delete', 'category-save-all-ranks',
      'disciple-get-all-data', 'disciple-filter-data', 'disciple-save', 'disciple-delete',
      'discat-get-all-data', 'discat-save',
      'setting-get-all-data', 'setting-changed',
      'import-csv', 'save-imported-results', 'import-results',
      'export-plain', 'export-category', 'export-formatted', 'export-roster'
    ]
    if (validChannels.includes(channel)) {
      return ipcRenderer.invoke(channel, data);
    }
  },
})

