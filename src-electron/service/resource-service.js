import { webContents } from 'electron';
import fs from 'fs';
import { DATABASES, db } from './db-access-service';
import settingService from './setting-service';
import discatService from './discat-service';
import stringService from './string-service';
import { runAsync } from './promise-service';

class ResourceService {
  printToPdf(callback) {
    webContents.printToPDF(
      {
        marginsType: 0,
        printBackground: false,
        printSelectionOnly: false,
        landscape: false,
      },
      (error, data) => {
        if (error) throw error;
        const settings = settingService.getSettings();
        fs.writeFile(`${settings.exportPath}/atletika.pdf`, data, (fserror) => {
          if (fserror) throw fserror;
          console.log('Write PDF successfully.');
          callback();
        });
      }
    );
  }

  saveToCsv(categoryId, exportFunction) {
    return db
      .filter(DATABASES.DISCIPLE, { categoryId: categoryId }, null)
      .docs.map((disc) => exportFunction(categoryId, disc))[0];
  }

  exportToCsv(categoryIds) {
    const dataPromises = categoryIds.map((ctg) =>
      runAsync(() => db.filter(DATABASES.DISCIPLE, ctg).docs)
        .then((respDisciple) => ({
          category: ctg.categoryId,
          disciples: respDisciple,
        }))
        .then((msg) => this.addDisciplesForCategory(msg))
        .then((msg) => this.addPersonsForCategory(msg))
    );

    return Promise.all(dataPromises)
      .then((msgList) =>
        msgList
          .map(
            (msg) =>
              `"${msg.category}"\n"Pořadí";"Start. číslo";"Závodník";"Příslušnost";"Rok nar."${msg.discipleNames};"Skóre"\n${msg.persons}`
          )
          .join('\n')
      )
      .then((outputStr) => {
        // TODO - sjednotit ve funkci writeToFile
        const settings = settingService.getSettings();
        fs.writeFile(
          `${settings.exportPath}/vysledky_komplet.csv`,
          outputStr,
          (fserror) => {
            if (fserror) throw fserror;
            console.log('Write CSV successfully.');
          }
        );

        return settings.exportPath;
      });
  }

  getResultsCategoryContent(categoryId) {
    const disciplePromise = runAsync(
      () => db.filter(DATABASES.DISCAT, { categoryId: categoryId }).docs
    ).then(
      (discat) =>
        db.filter(DATABASES.DISCIPLE, {
          _id: {
            $in: discatService.getDiscipleNamesForDiscat(
              discat && discat.length > 0 ? discat[0] : {}
            ),
          },
        }).docs
    );
    const personPromise = runAsync(
      () => db.filter(DATABASES.PERSON, { categoryId: categoryId }).docs
    ).then((respPerson) =>
      respPerson.sort((a, b) => a.finalOrder - b.finalOrder)
    );
    const categoryPromise = runAsync(() =>
      db.getById(DATABASES.CATEGORY, categoryId)
    );
    return Promise.all([disciplePromise, personPromise, categoryPromise]).then(
      (promiseResults) => ({
        disciples: promiseResults[0],
        persons: promiseResults[1],
        category: promiseResults[2],
        header: this.getResultsHeader(promiseResults[0]),
        rows: this.getResultsRows(promiseResults[0], promiseResults[1]),
      })
    );
  }

  getPlainResults() {
    const athletes = db.findAll(DATABASES.PERSON).docs;
    const categories = db.findAll(DATABASES.CATEGORY).docs;
    return {
      rows: this.getResultsRows([], athletes, categories),
      header: this.getResultsHeader([], true),
    };
  }

  getResultsHeader(disciples, withCategory = false) {
    let result = ['Pořadí', 'Start. číslo', 'Závodník'];
    if (withCategory) {
      result.push('Kategorie');
    }
    result.push('Příslušnost', 'Rok nar.');
    result = result.concat(disciples.map((disc) => disc.label));
    result.push('Skóre');
    return result;
  }

  getResultsRows(disciples, persons, categories = null) {
    return persons.map((person) =>
      this.getResultsOneRow(person, disciples, categories)
    );
  }

  getResultsOneRow(person, disciples, categories) {
    let result = [
      { value: person.finalOrder, type: 'number' },
      { value: person.startingNumber, type: 'number' },
      { value: `${person.lastName} ${person.firstName}`, type: 'text' },
    ];
    if (categories) {
      const category = categories.find((ctg) => ctg._id === person.categoryId);
      result.push({ value: category.name, type: 'text' });
    }
    result.push(
      { value: person.club, type: 'text' },
      { value: person.year, type: 'text' }
    );

    // ekvivalent disciples.flatMap(disciple => [person[disciple.colName], person[`${disciple.colName}_score`]]);
    const resultPairs = disciples.reduce(
      (acc, disciple) => acc.concat(this.getResultsOneCell(person, disciple)),
      []
    );
    result = result.concat(resultPairs);
    result.push({
      value: person.totalScore ? person.totalScore : 0,
      type: 'fp',
    });
    return result;
  }

  getResultsOneCell(person, disciple) {
    const scoreValue = person[`${disciple._id}_score`];
    const discipleValue = scoreValue === 0 ? 'DNF' : person[disciple._id];

    return [
      {
        value: person.resigned ? 'DNS' : discipleValue,
        type: person.resigned ? 'text' : disciple.datatype,
      },
    ];
  }

  addDisciplesForCategory(msg) {
    let discipleNames = '';
    msg.disciples.forEach((disciple) => {
      discipleNames += `;"${disciple.label}";""`;
    });
    msg.discipleNames = discipleNames;
    return {
      category: msg.category,
      disciples: msg.disciples,
      discipleNames: discipleNames,
    };
  }

  addPersonsForCategory(msg) {
    let output = '';
    return runAsync(
      () => db.filter(DATABASES.PERSON, { categoryId: msg.category }).docs
    )
      .then((respPerson) =>
        respPerson.sort((a, b) => a.finalOrder - b.finalOrder)
      )
      .then((rows) => {
        rows.forEach((person) => {
          output +=
            `"${person.finalOrder}";"${person._id}";"${person.lastName} ${person.firstName}";` +
            `"${person.club}";"${person.year}"`;
          output += this.addDisciplesToPersonRow(person, msg.disciples);
        });
        output += '\n';

        return {
          category: msg.category,
          disciples: msg.disciples,
          discipleNames: msg.discipleNames,
          persons: output,
        };
      });
  }

  addDisciplesToPersonRow(person, disciples) {
    let result = '';
    disciples.forEach((disciple) => {
      const personScore =
        typeof person[`${disciple.colName}_score`] !== 'undefined'
          ? person[`${disciple.colName}_score`]
          : 0;
      result += `;${ResourceService.formatValue(
        person,
        disciple
      )};${personScore}`;
    });
    result += `;${person.totalScore}\n`;
    return result;
  }

  writeToFile(output, prefix, discipleName) {
    const settings = settingService.getSettings();
    const exportPath = `${settings.exportPath}/${prefix}_${discipleName}.csv`;
    fs.writeFile(exportPath, output, (fserror) => {
      if (fserror) throw fserror;
      console.log('Write CSV successfully.');
    });

    return exportPath;
  }

  readFromFile(path) {
    return new Promise((resolve, reject) => {
      fs.readFile(path, 'utf8', (err, data) => {
        if (err) reject(err);

        resolve(data);
      });
    });
  }

  static formatValue(person, disciple) {
    if (person.resigned) {
      return 'DNS';
    } else if (person[`${disciple.colName}_score`] === 0) {
      return 'DNF';
    } else if (person[disciple.colName] == null) {
      return '-';
    } else if (disciple.datatype === 'time') {
      return stringService.formatTime(person[disciple.colName]);
    }

    return person[disciple.colName];
  }
}

export default new ResourceService();
