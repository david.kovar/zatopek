class MiscService {
  getOrDefault(value, defaultSupplier) {
    if (value) return value;

    return defaultSupplier();
  }
}

export default new MiscService();
