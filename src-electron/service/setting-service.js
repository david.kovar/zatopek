import { db, DATABASES } from './db-access-service';

const _SETTINGS_MAIN_ID = 'application';
const DEFAULT_SETTINGS = { exportPath: './output' };

class SettingService {
  getSettings() {
    try {
      const settings = db.getById(DATABASES.SETTING, _SETTINGS_MAIN_ID);
      return { ...DEFAULT_SETTINGS, ...settings };
    } catch (err) {
      console.log(`Error in getSettings: ${err}`);

      // apply defaults
      return DEFAULT_SETTINGS;
    }
  }

  save(setting) {
    if (!setting._id) setting._id = _SETTINGS_MAIN_ID;
    db.save(setting, DATABASES.SETTING);
  }
}

export default new SettingService();
export const SETTINGS_MAIN_ID = _SETTINGS_MAIN_ID;
