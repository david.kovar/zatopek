import _ from 'lodash';
import { db, DATABASES } from './db-access-service';
import excelService from './excel-service';
import resourceService from './resource-service';
import discatService from './discat-service';
import stringService from './string-service';
import { runAsync } from './promise-service';
import discipleService from './disciple-service';

const FP_NUMBER_REGEX = /\d+\.?\d*/;
const FP_NUMBER_COMMA_REGEX = /\d+,\d+/;
const SHORT_TIME_REGEX = /\d\d:\d\d/;
const LONG_TIME_REGEX = /\d\d:\d\d:\d\d/;
const ENCODED_ID_REGEX = /##[^#]+##/;

class ResultService {
  exportCategory(categoryId) {
    return this.computeScore(categoryId)
      .then(() => resourceService.getResultsCategoryContent(categoryId))
      .then((msg) => excelService.exportCategoryResults(msg));
  }

  exportFormatted(amount) {
    const computePromise = (categoryId) =>
      this.computeScore(categoryId).then(() =>
        resourceService.getResultsCategoryContent(categoryId)
      );

    return runAsync(() => {
      const categoryIds = db
        .filter(DATABASES.CATEGORY, {})
        .docs.sort((a, b) => a.rank - b.rank)
        .map((ctg) => ctg._id);

      const computePromises = categoryIds.map(computePromise);
      return Promise.all(computePromises)
        .then((msgList) =>
          amount === 'best'
            ? msgList.map(ResultService.filterBestPersons)
            : msgList
        )
        .then((msgList) => excelService.exportAllResults(msgList));
    });
  }

  importResults(data) {
    return excelService
      .importExcel(data)
      .then(
        (content) =>
          content['office:document-content']['office:body'][0][
            'office:spreadsheet'
          ][0]['table:table'][0]['table:table-row']
      )
      .then((rows) => this.extractDataFromTable(rows));
  }

  saveImportedResults(results) {
    return runAsync(() => {
      if (results && results.length > 0) {
        results.forEach((category) => this.saveImportedCategory(category));
      }
    });
  }

  exportPlain() {
    this.computeScoreForAllCategories();
    const results = resourceService.getPlainResults();
    return excelService.exportPlainResults(results);
  }

  computeScoreForAllCategories() {
    return db
      .findAll(DATABASES.CATEGORY)
      .docs.map((ctg) =>
        this.computeScore(ctg._id).then(() =>
          resourceService.getResultsCategoryContent(ctg._id)
        )
      );
  }

  computeScore(categoryId) {
    const personPromise = runAsync(
      () => db.filter(DATABASES.PERSON, { categoryId: categoryId }).docs
    );
    const discPromise = runAsync(
      () => db.filter(DATABASES.DISCAT, { categoryId: categoryId }).docs
    ).then(
      (discat) =>
        db.filter(DATABASES.DISCIPLE, {
          _id: {
            $in: discatService.getDiscipleNamesForDiscat(
              discat && discat.length > 0 ? discat[0] : {}
            ),
          },
        }).docs
    );

    return Promise.all([personPromise, discPromise]).then((promiseResult) => {
      const persons = promiseResult[0];
      const disciples = promiseResult[1];

      disciples.forEach((disc) => {
        this.computeScoreForDisciple(disc._id, disc.direction, persons);
      });
      persons.forEach((person) => {
        this.computeTotalScore(person, disciples);
      });

      ResultService.computeFinalOrder(persons);

      return runAsync(() => db.saveAll(persons, DATABASES.PERSON));
    });
  }

  computeScoreForDisciple(colName, direction, persons) {
    // if there is no person, we have no work
    if (persons.length === 0) return;
    const activePersons = persons.filter((person) => !person.resigned);

    const rankWeight = 0.75;
    const resultWeight = 1 - rankWeight;

    const greater = direction === 'a' ? 1 : -1;
    const lesser = direction === 'a' ? -1 : 1;
    const noValue = direction === 'a' ? 9999 : 0;
    activePersons.sort((a, b) => {
      const aValue = parseFloat(a[colName] != null ? a[colName] : noValue);
      const bValue = parseFloat(b[colName] != null ? b[colName] : noValue);
      if (aValue > bValue) {
        return greater;
      } else if (bValue > aValue) {
        return lesser;
      }
      return 0;
    });

    let highestResult = ResultService.purifyNumber(
      direction === 'a'
        ? activePersons[0][colName]
        : activePersons[activePersons.length - 1][colName]
    );
    let lowestResult = ResultService.purifyNumber(
      direction === 'a'
        ? activePersons[activePersons.length - 1][colName]
        : activePersons[0][colName]
    );

    if (highestResult == null) {
      highestResult = 9999 - noValue;
    }
    if (lowestResult == null) {
      lowestResult = noValue;
    }
    const percentDivider = highestResult - lowestResult;
    const scoreColName = `${colName}_score`;
    let rank = 1;
    let realRank = 1;
    let discipleValue = noValue;
    for (let i = activePersons.length - 1; i >= 0; i -= 1) {
      const prevDiscipleValue = discipleValue;
      discipleValue = ResultService.purifyNumber(
        activePersons[i][colName],
        noValue
      );

      const percentNumerator =
        direction === 'a'
          ? discipleValue - lowestResult
          : highestResult - discipleValue;
      const percent =
        percentDivider !== 0 ? percentNumerator / percentDivider : 0;
      if (i >= 0 && discipleValue !== prevDiscipleValue) {
        rank = realRank;
      }
      const score =
        discipleValue === noValue
          ? 0
          : rank * rankWeight + activePersons.length * percent * resultWeight;

      realRank += 1;
      activePersons[i][scoreColName] = ResultService.roundSmallNumber(score, 2);
    }
  }

  computeTotalScore(person, cols) {
    let totalScore = 0;
    cols.forEach((col) => {
      totalScore +=
        person[`${col._id}_score`] > 0 ? person[`${col._id}_score`] : 0;
    });
    person.totalScore = totalScore;
  }

  extractDataFromTable(rows) {
    const categories = [];
    let oneCategory = {};
    let lastHeader = [];
    for (let i = 0; i < rows.length; i += 1) {
      if (
        rows[i]['table:table-cell'][0].$['table:number-columns-spanned'] != null
      ) {
        // now you are in category label row
        if (!_.isEmpty(oneCategory)) {
          categories[categories.length] = oneCategory;
        }
        oneCategory = {
          header: {
            label: rows[i]['table:table-cell'][0]['text:p'][0],
            id: ResultService.getAnnotationContent(
              rows[i]['table:table-cell'][0]
            ),
          },
          columns: [],
          rows: [],
        };

        const disciples = discipleService.getDisciplesForCategory(
          oneCategory.header.id
        );
        if (rows.length >= i + 1) {
          i += 1;
          oneCategory.columns = rows[i]['table:table-cell']
            .filter((cell) => cell != null && cell['text:p'] != null)
            .map((cell) => this.extractTableHeader(cell, disciples));
        }
        lastHeader = oneCategory.columns;
      } else {
        // now you are in row with results
        const result = {};
        rows[i]['table:table-cell'].forEach((cell, idx) => {
          if (idx < lastHeader.length && lastHeader[idx] != null) {
            result[lastHeader[idx].name] = ResultService.convertValue(cell);
          }
        });
        oneCategory.rows[oneCategory.rows.length] = result;
      }
    }

    // add last category
    if (!_.isEmpty(oneCategory)) {
      categories[categories.length] = oneCategory;
    }

    return categories;
  }

  extractTableHeader(cell, disciples) {
    const discipleId = ResultService.getAnnotationContent(cell);
    return {
      name: discipleId,
      field: discipleId,
      datatype: ResultService.getDiscipleDatatype(disciples, discipleId),
      label: cell['text:p'][0],
    };
  }

  saveImportedCategory(category) {
    if ('header' in category && 'columns' in category && 'rows' in category) {
      category.rows.forEach((row) => {
        const persons = db.filter(DATABASES.PERSON, {
          startingNumber: String(row.startingNumber),
        }).docs;
        let changed = false;

        if (persons && persons.length > 0) {
          for (const [key, value] of Object.entries(row)) {
            if (
              !['startingNumber', 'fullName'].includes(key) &&
              value !== persons[0][key]
            ) {
              persons[0][key] = value;
              changed = true;
            }
          }

          if (changed) {
            db.save(persons[0], DATABASES.PERSON);
          }
        }
      });
    }
  }

  static getDiscipleDatatype(disciples, discipleId) {
    const found = disciples.find((disc) => disc._id === discipleId);
    return found ? found.datatype : '';
  }

  static convertValue(cell) {
    const strValue = cell && cell['text:p'] ? cell['text:p'][0] : '';
    if (SHORT_TIME_REGEX.test(strValue) || LONG_TIME_REGEX.test(strValue)) {
      return stringService.parseTime(strValue);
    }
    if (FP_NUMBER_COMMA_REGEX.test(strValue)) {
      return parseFloat(strValue.replace(',', '.'));
    }
    if (FP_NUMBER_REGEX.test(strValue)) {
      return parseFloat(strValue);
    }

    return strValue;
  }

  static computeFinalOrder(persons) {
    persons.sort((a, b) => {
      if (a.totalScore > b.totalScore) {
        return -1;
      } else if (b.totalScore > a.totalScore) {
        return 1;
      }
      return 0;
    });

    let order = 1;
    let realOrder = 1;

    for (let i = 0; i < persons.length; i += 1) {
      persons[i].finalOrder = order;
      realOrder += 1;
      if (
        i < persons.length - 1 &&
        persons[i].totalScore !== persons[i + 1].totalScore
      ) {
        order = realOrder;
      }
    }
  }

  // filter only best 3 persons to the output
  static filterBestPersons(msg) {
    msg.rows = msg.rows.filter((row) => row[0].value <= 3);
    return msg;
  }

  static roundSmallNumber(number, digits) {
    const modulus = 10 ** digits;
    return Math.round(number * modulus) / modulus;
  }

  static getAnnotationContent(annotationObject) {
    const result = ENCODED_ID_REGEX.exec(JSON.stringify(annotationObject));
    return result == null ? null : result[0].replaceAll('##', '');
  }

  static purifyNumber(input, noValue) {
    const bestValue = 9999 - noValue;
    const fvalue = parseFloat(input);
    const value = Number.isNaN(fvalue) ? noValue : fvalue;
    return value === bestValue ? noValue : value;
  }
}

export default new ResultService();
