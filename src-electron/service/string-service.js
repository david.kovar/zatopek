import diacritics from 'diacritics';

class StringService {
  formatTime(strTime, fillMinutes = false) {
    const numTime = Number(strTime);
    if (!numTime) return '00:00';

    const hour = Math.floor(numTime / 3600);
    const min = Math.floor((numTime % 3600) / 60);
    const sec = numTime % 60;

    let result = this.fillZero(sec);
    if (numTime > 60) {
      if (fillMinutes || numTime > 3600) {
        result = `${this.fillZero(min)}:${result}`;
      } else {
        result = `${min}:${result}`;
      }
    }
    if (numTime > 3600) {
      result = `${hour}:${result}`;
    }

    return result;
  }

  formatOfficeTime(strTime) {
    const numTime = Number(strTime);
    if (!numTime) return '00:00';

    const hour = Math.floor(numTime / 3600);
    const min = Math.floor((numTime % 3600) / 60);
    const sec = numTime % 60;
    return `PT${this.fillZero(hour)}H${this.fillZero(min)}M${this.fillZero(
      sec
    )}S`;
  }

  parseTime(strTime) {
    const timeSegments = strTime.split(':');

    if (timeSegments.length === 2) {
      return 60 * parseInt(timeSegments[0], 10) + parseInt(timeSegments[1], 10);
    }
    if (timeSegments.length === 3) {
      return (
        3600 * parseInt(timeSegments[0], 10) +
        60 * parseInt(timeSegments[1], 10) +
        parseInt(timeSegments[2], 10)
      );
    }

    console.log(`Unknown time format: ${strTime}`);
    return 0;
  }

  fillZero(num) {
    const str = `${num}`;
    if (str.length === 0) {
      return '00';
    }
    if (str.length === 1) {
      return `0${str}`;
    }
    return str;
  }

  toFieldName(label) {
    const unAccented = diacritics.remove(label);
    const unAccentedChars = unAccented.split('');
    const output = [];
    let lastchar = '';
    for (let i = 0; i < unAccented.length; i += 1) {
      const char = unAccented.charCodeAt(i);
      if (
        (char >= 48 && char <= 57) || // numbers
        (char >= 65 && char <= 90) || // capitals
        (char >= 97 && char <= 122)
      ) {
        // letters
        output.push(unAccentedChars[i]);
        lastchar = unAccentedChars[i];
      } else if (lastchar !== '_') {
        output.push('_');
        lastchar = '_';
      }
    }

    return output.join('');
  }
}

export default new StringService();
