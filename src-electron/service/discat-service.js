import { db, DATABASES } from './db-access-service';

class DiscatService {
  mapToDiscatId(discipleId) {
    return `disc_${discipleId}`;
  }

  getDiscipleNamesForDiscat(discat) {
    return (
      Object.keys(discat)
        .filter((key) => key.startsWith('disc_'))
        // filter out these which are not checked
        .filter((disciple) => discat[disciple])
        // crop leading 'disc_'
        .map((disciple) => disciple.substr(5))
    );
  }

  saveDiscats(discats) {
    return new Promise((resolve) => {
      discats.forEach((discat) => {
        delete discat.__index;
        db.save(discat, DATABASES.DISCAT);
      });

      resolve();
    });
  }

  getAllDiscats() {
    const categories = db.findAll(DATABASES.CATEGORY).docs;
    return db.findAll(DATABASES.DISCAT).docs.map((discat) => {
      const category = categories.filter(
        (cat) => cat._id === discat.categoryId
      );
      discat.categoryName =
        category.length > 0 ? category[0].name : discat.categoryId;
      return discat;
    });
  }
}

export default new DiscatService();
