import { db, DATABASES } from './db-access-service';
import categoryService from './category-service';
import resourceService from './resource-service';
import csvService from './csv-service';

class PersonService {
  findMatching(selector, sort) {
    const persons = db.filter(DATABASES.PERSON, selector).docs;
    if (sort) {
      persons.sort((a, b) => {
        if (!a && !a.lastName && !a.firstName) return -1;
        if (!b && !b.lastName && !b.firstName) return 1;

        return `${a.lastName}${a.firstName}`.localeCompare(
          `${b.lastName}${b.firstName}`
        );
      });
    }
    return persons;
  }

  savePerson(person, fillCategories) {
    db.save(person, DATABASES.PERSON);
    if (fillCategories) {
      categoryService.fillCategories();
    }
  }

  savePersons(personList, fillCategories) {
    db.saveAll(personList, DATABASES.PERSON);

    if (fillCategories) {
      categoryService.fillCategories();
    }
  }

  importCsv(path) {
    return resourceService
      .readFromFile(path)
      .then((content) => this.importPersons(content));
  }

  importPersons(content) {
    let rowCounter = 1;
    const persons = csvService
      .parseCsv(content)
      .map((row) => this.rowToPerson(row, (rowCounter += 1)));

    this.savePersons(persons, true);
  }

  rowToPerson(row, i) {
    const startingNumber = row[0];
    const year = parseInt(row[3], 10);
    const sexOrig = row[4] ? row[4].toLowerCase() : 'm';
    const sex = sexOrig === 'm' || sexOrig === 'f' ? sexOrig : 'm';

    if (Number.isNaN(year)) throw new Error(`ERR[${i};year]:Not a number`);

    // lookup for categoryId
    let categoryId = [];
    if (row[7]) {
      categoryId = db.filter(
        DATABASES.CATEGORY,
        { name: row[7] },
        null,
        1
      ).docs;
      if (categoryId.length === 0) {
        throw new Error(
          `ERR[${i};manual_category]:'${row[7]}' is not defined in category list`
        );
      }
    }

    return {
      startingNumber: startingNumber,
      lastName: row[1],
      firstName: row[2],
      year: year,
      sex: sex,
      email: row[5],
      club: row[6],
      manualCategoryId: categoryId.length > 0 ? categoryId[0]._id : null,
      categoryId: categoryId.length > 0 ? categoryId[0]._id : null,
      resigned: false,
    };
  }
}

export default new PersonService();
