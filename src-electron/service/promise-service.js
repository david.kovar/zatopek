import xml2js from 'xml2js';
import fs from 'fs';

export function runAsync(runnable) {
  return new Promise((resolve, reject) => {
    try {
      resolve(runnable());
    } catch (e) {
      reject(e);
    }
  });
}

export const promiseParseXml = (content) =>
  new Promise((resolve, reject) => {
    xml2js.parseString(content, (error, result) => {
      if (error) reject(error);
      else resolve(result);
    });
  });

export const promiseReadFile = (path) =>
  new Promise((resolve, reject) => {
    fs.readFile(path, (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
