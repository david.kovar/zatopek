import { db, DATABASES } from './db-access-service';
import discatService from './discat-service';
import Discat from '../../src/model/discat';
import Category from '../../src/model/category';
// TODO shared model for Renderer and Main process
// import Category, { SEX_ENUM } from 'src/model/category';
const SEX_ENUM = Object.freeze({ MALE: 'm', FEMALE: 'f', BOTH: 'b' });

class CategoryService {
  createDefault() {
    const category = { name: 'Default', ageTo: 0, sex: SEX_ENUM.BOTH };
    return this.save(category);
  }

  save(category) {
    if (category.extra) {
      category.ageTo = 9999;
    } else {
      category.extra = false;
    }

    if (!category.sex) category.sex = SEX_ENUM.BOTH;
    if (!category.rank) category.rank = this.getNextCategoryRank();
    if (!category.ageTo) {
      category.ageTo = 0;
    } else {
      try {
        category.ageTo = parseInt(category.ageTo, 10);
      } catch {
        throw new Error(`${category.ageTo} is not a valid date`);
      }
    }

    const obj = this.mapToCategory(category);
    const categoryWithId = db.save(obj, DATABASES.CATEGORY);
    this.connectDisciples(category);
    this.calculateAgeFrom();

    return categoryWithId;
  }

  saveAllRanks(categories) {
    db.saveAll(categories, DATABASES.CATEGORY);
  }

  delete(category) {
    db.delete(category, DATABASES.CATEGORY);
    this.disconnectDisciples(category);
  }

  fillCategories() {
    const respPerson = db.findAll(DATABASES.PERSON);
    const maleCats = db.filter(
      DATABASES.CATEGORY,
      { sex: { $in: ['m', 'b'] } },
      { ageTo: 'desc' }
    ).docs;
    const femaleCats = db.filter(
      DATABASES.CATEGORY,
      { sex: { $in: ['f', 'b'] } },
      { ageTo: 'desc' }
    ).docs;
    return respPerson.docs.map((person) =>
      this.fillCategoryToPerson(person, maleCats, femaleCats)
    );
  }

  fillCategoryToPerson(person, maleCategories, femaleCategories) {
    // do not automatically recalculate manually selected categories
    if (!person.manualCategoryId) {
      const categories = person.sex === 'f' ? femaleCategories : maleCategories;

      let i = 0;
      // policy to prevent infinite loop if Default category is missing
      while (
        i < categories.length &&
        (categories[i].ageTo > person.year ||
          (categories[i].sex !== person.sex && categories[i].sex !== 'b'))
      ) {
        i += 1;
      }

      if (person.categoryId !== categories[i]._id) {
        person.categoryId = categories[i]._id;

        try {
          db.save(person, DATABASES.PERSON);
        } catch (err) {
          console.log(
            `Error when updating database ${DATABASES.PERSON}: ${err}`
          );
        }
      }
    }
  }

  exportRoster(categoryList) {
    // add disciples to each category
    return db
      .filter(DATABASES.DISCAT, { categoryId: { $in: categoryList } })
      .docs.map((discat) => {
        const discNames = discatService.getDiscipleNamesForDiscat(discat);
        // add disciples to each category
        discat.disciples = db.filter(DATABASES.DISCIPLE, {
          _id: { $in: discNames },
        }).docs;
        // add persons to each category
        discat.persons = db
          .filter(DATABASES.PERSON, { categoryId: discat.categoryId })
          .docs.filter((p) => !p.resigned);
        // add detail info from category
        const ctgDetail = db.getById(DATABASES.CATEGORY, discat.categoryId);
        discat.ageFrom = ctgDetail.ageFrom;
        discat.ageTo = ctgDetail.ageTo;
        discat.extra = ctgDetail.extra;
        return discat;
      });
  }

  connectDisciples(category) {
    const discat = db.filter(DATABASES.DISCAT, {
      categoryId: category._id,
    }).docs;
    // don't connect already existing category
    if (discat.length > 0) return;

    const newDiscat = this.mapToDiscat(
      category,
      db.findAll(DATABASES.DISCIPLE).docs
    );

    db.save(newDiscat, DATABASES.DISCAT);
  }

  calculateAgeFrom() {
    this._calculateAgeFrom('m');
    this._calculateAgeFrom('f');
  }

  _calculateAgeFrom(sex) {
    const selector = { sex: { $in: [sex, 'b'] } };
    const sort = { ageTo: 'asc' };
    return db
      .filter(DATABASES.CATEGORY, selector, sort)
      .docs.filter((category) => !category.extra)
      .forEach((current, index, array) => {
        db.modify(DATABASES.CATEGORY, current._id, (oldCtg) => {
          oldCtg.ageFrom = array[index + 1]
            ? parseInt(array[index + 1].ageTo, 10) - 1
            : new Date().getFullYear();
          return oldCtg;
        });
      });
  }

  disconnectDisciples(category) {
    db.filter(DATABASES.DISCAT, { categoryId: category }).docs.forEach(
      (discat) => db.delete(discat._id, DATABASES.DISCAT)
    );
  }

  mapToCategory(newCategory) {
    if (newCategory._id !== '') return newCategory;

    const oldCategory = db.filter(
      DATABASES.CATEGORY,
      { name: newCategory.name },
      null,
      1
    );
    const result =
      oldCategory.docs.length > 0 ? oldCategory.docs[0] : new Category();
    result.name = newCategory.name;
    result.ageTo = newCategory.ageTo;
    result.sex = newCategory.sex;
    result.extra = newCategory.extra;
    result.rank = newCategory.rank;
    return result;
  }

  mapToDiscat(category, disciples) {
    const result = new Discat(category._id);
    disciples.reduce((discat, disc) => {
      discat[discatService.mapToDiscatId(disc._id)] = false;
      return discat;
    }, result);
    return result;
  }

  getNextCategoryRank() {
    const lastCategory = db.filter(
      DATABASES.CATEGORY,
      {},
      { rank: 'desc' },
      1
    ).docs;
    if (
      !lastCategory ||
      lastCategory.length === 0 ||
      typeof lastCategory[0].rank !== 'number'
    ) {
      return 1;
    }

    return lastCategory[0].rank + 1;
  }
}

export default new CategoryService();
