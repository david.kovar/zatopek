import GooDb from 'goodb';

const DB_DIRECTORY = './db';

const _DATABASES = Object.freeze({
  PERSON: 'person',
  CATEGORY: 'cat',
  DISCIPLE: 'disc',
  DISCAT: 'discat',
  SETTING: 'setting',
});

class DbAccessService {
  constructor() {
    this.dbs = {};

    this.dbs[_DATABASES.PERSON] = new GooDb(_DATABASES.PERSON, {
      root: DB_DIRECTORY,
    });
    this.dbs[_DATABASES.CATEGORY] = new GooDb(_DATABASES.CATEGORY, {
      root: DB_DIRECTORY,
    });
    this.dbs[_DATABASES.DISCIPLE] = new GooDb(_DATABASES.DISCIPLE, {
      root: DB_DIRECTORY,
    });
    this.dbs[_DATABASES.DISCAT] = new GooDb(_DATABASES.DISCAT, {
      root: DB_DIRECTORY,
    });
    this.dbs[_DATABASES.SETTING] = new GooDb(_DATABASES.SETTING, {
      root: DB_DIRECTORY,
    });
  }

  // response is: { totalDocs:1, offset:0, pageSize: 0, docs: [Object] }
  findAll(dbName) {
    const db = this.getDbByName(dbName);
    try {
      return db.findAll();
    } catch (err) {
      console.log(`Error: ${err}`);
      throw new Error(`Error when get allDocs from DB '${dbName}': ${err}`);
    }
  }

  // response is: { totalDocs:1, offset:0, pageSize: 0, docs: [Object] }
  filter(dbName, selector, sort, limit) {
    const db = this.getDbByName(dbName);

    const request = {
      selector: selector,
    };
    if (sort) {
      request.sort = sort;
    }
    if (limit) {
      request.limit = limit;
    }

    try {
      return db.filter(request);
    } catch (err) {
      console.log(err);
      throw new Error(`Error when get filter from DB '${dbName}': ${err}`);
    }
  }

  getById(dbName, id) {
    const db = this.getDbByName(dbName);

    return db.findById(id);
  }

  save(obj, dbName) {
    const db = this.getDbByName(dbName);
    try {
      return db.upsert(obj, { consistent: true });
    } catch (err) {
      console.log(err);
      throw new Error(
        `Error when save object <${JSON.stringify(
          obj
        )}> to DB '${dbName}': ${err}`
      );
    }
  }

  saveAll(objList, dbName) {
    const db = this.getDbByName(dbName);
    try {
      return db.upsertAll(objList, { consistent: true });
    } catch (err) {
      console.log(err);
      throw new Error(`Error when saveAll objects to DB '${dbName}': ${err}`);
    }
  }

  modify(dbName, id, modificatorFunction) {
    const db = this.getDbByName(dbName);
    return db.modifyById(id, modificatorFunction, { consistent: true });
  }

  delete(docId, dbName) {
    const db = this.getDbByName(dbName);
    if (docId == null || docId === '') {
      throw new Error('Empty _id. What should be deteleted?');
    }

    try {
      return db.deleteById(docId, { consistent: true });
    } catch (err) {
      console.log(`Error: ${err}`);
      throw new Error(
        `Error when deleting row ${docId} from DB '${dbName}': ${err}`
      );
    }
  }

  getDbByName(name) {
    if (Object.prototype.hasOwnProperty.call(this.dbs, name)) {
      return this.dbs[name];
    }

    console.log(`Can't find DB with name ${name}`);
    throw new Error(`Can't find DB with name ${name}`);
  }
}

export default new DbAccessService();
export const db = new DbAccessService();
export const DATABASES = _DATABASES;
