import os from 'os';

const defaults = {
  escape: '"',
  separator: ';',
  newline: os.EOL,
};

class CsvService {
  constructor(opts = {}) {
    this.options = Object.assign({}, defaults, opts);
  }

  parseCsv(buffer) {
    const allRows = [];
    let state = 1; // 1=outside quotes; 2=inside quotes; 3=after closed quotes
    let row = [];
    let cell = '';

    for (let i = 0; i < buffer.length; i++) {
      if (buffer[i] === this.options.escape) {
        if (state === 1) {
          // switch to state "inside quotes"
          state = 2;
          // ignore everything before first quote in cell
          cell = '';
        } else if (state === 2) {
          // switch to state "after closed quotes"
          state = 3;
        }
      } else if (buffer[i] === this.options.separator) {
        if (state === 1) {
          row[row.length] = cell.trim();
          cell = '';
        } else if (state === 2) {
          cell += buffer[i];
        } else if (state === 3) {
          row[row.length] = cell.trim();
          cell = '';
          state = 1;
        }
      } else if (buffer[i] === this.options.newline) {
        if (state === 1) {
          row[row.length] = cell.trim();
          allRows[allRows.length] = row;
          row = [];
          cell = '';
        } else if (state === 2) {
          // skip newline in quotes
          cell += ' ';
        } else if (state === 3) {
          row[row.length] = cell.trim();
          allRows[allRows.length] = row;
          row = [];
          cell = '';
          state = 1;
        }
      } else if (state === 1 || state === 2) {
        // all other characters add to cell
        cell += buffer[i];
      }
    }

    return allRows;
  }
}

export default new CsvService();
