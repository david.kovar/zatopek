import { db, DATABASES } from './db-access-service';
import stringService from './string-service';
import discatService from './discat-service';

class DiscipleService {
  getDisciplesForCategory(categoryId) {
    const discats = db.filter(DATABASES.DISCAT, {
      categoryId: categoryId,
    }).docs;
    const disciplesInCategory = this.getDiscipleNamesFromDiscat(discats);
    const disciples = db.findAll(DATABASES.DISCIPLE).docs;

    // return only disciples which are selected in discats
    return disciples.filter((disciple) =>
      disciplesInCategory.includes(disciple._id)
    );
  }

  save(disciple) {
    disciple.colName = stringService.toFieldName(disciple.label);
    db.save(disciple, DATABASES.DISCIPLE);
    this.connectCategories(disciple);
  }

  delete(disciple) {
    db.delete(disciple, DATABASES.DISCIPLE);
    this.disconnectCategories(disciple);
  }

  connectCategories(disciple) {
    db.findAll(DATABASES.DISCAT).docs.forEach((oneDiscat) => {
      oneDiscat[discatService.mapToDiscatId(disciple._id)] = false;
      db.save(oneDiscat, DATABASES.DISCAT);
    });
  }

  disconnectCategories(disciple) {
    const discats = db.findAll(DATABASES.DISCAT);
    discats.docs.forEach((oneDiscat) => {
      delete oneDiscat[discatService.mapToDiscatId(disciple)];
      db.save(oneDiscat, DATABASES.DISCAT);
    });
  }

  getDiscipleNamesFromDiscat(discatArray) {
    if (!discatArray || discatArray.length === 0) return [];

    return discatService.getDiscipleNamesForDiscat(discatArray[0]);
  }
}

export default new DiscipleService();
