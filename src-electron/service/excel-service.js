import JSZip from 'jszip';
import xml2js from 'xml2js';
import fs from 'fs';
import stringService from './string-service';
import settingService from './setting-service';
import categoryService from './category-service';
import personService from './person-service';
import { runAsync, promiseParseXml, promiseReadFile } from './promise-service';

const TEMPLATE_PATH = './resources/templates';
const TEMPLATE_ROSTER = 'roster.ods';
const TEMPLATE_RESULTS = 'results.ods';
const TEMPLATE_PLAIN = 'plain_results.ods';
const ODS_CONTENT = 'content.xml';
// const EMPTY_ROW = { 'table:table-row': [{ 'table:table-cell': [''] }] };
const MAX_FILENAME_LEN = 64;

class ExcelService {
  openExcel(data, internalFileName) {
    const zip = new JSZip();

    return zip
      .loadAsync(data)
      .then(() => zip.file(internalFileName).async('nodebuffer'))
      .then((content) => content.toString('utf8'))
      .then((content) => promiseParseXml(content));
  }

  readFile(path, fileName, internalFileName) {
    return promiseReadFile(`${path}/${fileName}`).then((data) =>
      this.openExcel(data, internalFileName)
    );
  }

  writeFile(jsonSpreadsheet, path, fileName, internalFileName, outputFileName) {
    const zip = new JSZip();
    return promiseReadFile(`${path}/${fileName}`)
      .then((data) => zip.loadAsync(data))
      .then(() => {
        const builder = new xml2js.Builder();
        return builder.buildObject(jsonSpreadsheet);
      })
      .then((xmlWb) => zip.file(internalFileName, xmlWb))
      .then(() => settingService.getSettings())
      .then((setting) =>
        zip
          .generateNodeStream({ type: 'nodebuffer', streamFiles: true })
          .pipe(fs.createWriteStream(`${setting.exportPath}/${outputFileName}`))
          .on('finish', () => {
            // JSZip generates a readable stream with a "end" event,
            // but is piped here in a writable stream which emits a "finish" event.
            console.log(`${setting.exportPath}/${outputFileName} written.`);
          })
      )
      .then((zipObj) => zipObj.path);
  }

  exportRoster(categoryIds) {
    // read data structures to export from DB
    return (
      runAsync(() => categoryService.exportRoster(categoryIds))
        // read content of roster.ods
        .then((categoryList) =>
          this.getTemplateRows(TEMPLATE_ROSTER).then((wbJson) => ({
            categoryList,
            wbJson,
          }))
        )
        .then((rosterObj) => this.placeRowsToRoster(rosterObj))
        .then((rosterObj) =>
          this.writeFile(
            rosterObj.wbJson.contentJson,
            TEMPLATE_PATH,
            TEMPLATE_ROSTER,
            ODS_CONTENT,
            this.getRosterFilename(rosterObj)
          )
        )
    );
  }

  getRosterFilename(rosterObj) {
    const concat = `${rosterObj.categoryList
      .map((ctg) => ctg.categoryName)
      .join('+')}`;
    const result =
      concat.length > MAX_FILENAME_LEN
        ? `${concat.substr(0, MAX_FILENAME_LEN - 3)}...`
        : concat;
    return `${result}.ods`;
  }

  exportCategoryResults(msg) {
    return this.getTemplateRows(TEMPLATE_RESULTS)
      .then((wbJson) => this.placeRowsToResults(wbJson, msg))
      .then((wbJson) =>
        this.writeFile(
          wbJson.contentJson,
          TEMPLATE_PATH,
          TEMPLATE_RESULTS,
          ODS_CONTENT,
          `${msg.category.name}.ods`
        )
      );
  }

  exportAllResults(msgList) {
    return this.getTemplateRows(TEMPLATE_RESULTS)
      .then((wbJson) => {
        const filterdMsgList = msgList.filter(
          (msg) => msg.disciples.length > 0 && msg.persons.length > 0
        );
        return this.placeMultipleCategoriesToResults(wbJson, filterdMsgList);
      })
      .then((wbJson) =>
        this.writeFile(
          wbJson.contentJson,
          TEMPLATE_PATH,
          TEMPLATE_RESULTS,
          ODS_CONTENT,
          'results.ods'
        )
      );
  }

  exportPlainResults(results) {
    return this.getTemplateRows(TEMPLATE_PLAIN)
      .then((wbJson) => {
        wbJson.contentJson['office:document-content']['office:body'][0][
          'office:spreadsheet'
        ][0]['table:table'][0]['table:table-row'] = this.placePlainResults(
          wbJson,
          results
        );
        return wbJson;
      })
      .then((wbJson) =>
        this.writeFile(
          wbJson.contentJson,
          TEMPLATE_PATH,
          TEMPLATE_PLAIN,
          ODS_CONTENT,
          'plain_results.ods'
        )
      );
  }

  importExcel(odsContent) {
    return this.openExcel(odsContent, ODS_CONTENT);
  }

  placeRowsToResults(wbJson, data) {
    wbJson.contentJson['office:document-content']['office:body'][0][
      'office:spreadsheet'
    ][0]['table:table'][0]['table:table-row'] = this.placeOneCategoryToResults(
      data,
      wbJson
    );

    return wbJson;
  }

  placeMultipleCategoriesToResults(wbJson, categories) {
    let rows =
      wbJson.contentJson['office:document-content']['office:body'][0][
        'office:spreadsheet'
      ][0]['table:table'][0]['table:table-row'];
    rows = rows
      .filter((row) => !this.rowContainsText(row, '@@HEADER_ROW@@'))
      .filter((row) => !this.rowContainsText(row, '@@LABEL_ROW@@'))
      .flatMap((row) => {
        if (this.rowContainsText(row, '@@DATA_ROW@@')) {
          return categories.reduce(
            (acc, category) =>
              acc.concat(
                this.placeOneCategoryToResults(category, wbJson, true)
              ),
            []
          );
        }
        return row;
      })
      .map((row) => {
        if (this.rowContainsText(row, '@@SUMMARY_ROW@@')) {
          return this.getSummaryRow(row);
        }

        return row;
      });

    //     && !this.rowContainsText(row, '@@SUMMARY_ROW@@')
    //     && !this.rowContainsText(row, '@@TEAM_SUMMARY_ROW@@'))

    wbJson.contentJson['office:document-content']['office:body'][0][
      'office:spreadsheet'
    ][0]['table:table'][0]['table:table-row'] = rows;
    return wbJson;
  }

  getSummaryRow(row) {
    const strRow = JSON.stringify(row);
    const message = this.computePersonSummary();
    return JSON.parse(strRow.replace('@@SUMMARY_ROW@@', message));
  }

  computePersonSummary() {
    const sum = personService
      .findMatching({ resigned: { $ne: true } })
      .reduce(
          (acc, curr) => {
            acc.total += 1;
            if (new Date().getFullYear() - curr.year < 18) acc.children += 1;
            else if (curr.sex === 'f') acc.female += 1;
            else acc.male += 1;

            return acc;
          },
          {
            total: 0,
            children: 0,
            female: 0,
            male: 0,
          }
        );

    return `Celkem závodníků ${sum.total} (z toho ${sum.male} mužů, ${sum.female} žen a  ${sum.children} mládeže)`;
  }

  placeOneCategoryToResults(data, wbJson, withPlainRow = false) {
    const result = [];
    this.generateResultsHeading(wbJson, data, result);

    // export names
    data.rows.forEach((dataRow) => {
      const row = this.getEmptyRow(wbJson.dataCell);
      dataRow.forEach((value) => {
        // for each table column put to output formated data cell
        row['table:table-cell'].push(
          this.getCellWithValue(wbJson.dataCell, value)
        );
      });
      result.push(row);
    });

    if (withPlainRow) {
      const plainCell = wbJson.labelRow;
      plainCell['table:table-cell'][0].$['table:number-columns-spanned'] =
        data.header.length;
      result.push(this.getRowWithValue(plainCell, ''));
    }

    return result;
  }

  generateResultsHeading(wbJson, data, result) {
    // export label
    const { labelRow } = wbJson;
    labelRow['table:table-cell'][0].$['table:number-columns-spanned'] =
      data.header.length;
    result.push(this.getRowWithValue(labelRow, data.category.name));
    const header = this.getCellWithValue(wbJson.headerCell, '');
    header['table:table-cell'] = [];

    // export table level
    data.header.forEach((headerLabel) => {
      header['table:table-cell'].push(
        this.getCellWithValue(wbJson.headerCell, {
          value: headerLabel,
          type: 'text',
        })
      );
    });
    result.push(header);
  }

  placePlainResults(wbJson, results) {
    const result = [];

    // export table header
    const header = this.getCellWithValue(wbJson.headerCell, '');
    header['table:table-cell'] = [];
    results.header.forEach((headerCell) => {
      header['table:table-cell'].push(
        this.getCellWithValue(wbJson.headerCell, {
          value: headerCell,
          type: 'text',
        })
      );
    });
    result.push(header);

    // export names
    results.rows.forEach((dataRow) => {
      const row = this.getEmptyRow(wbJson.dataCell);
      dataRow.forEach((value) => {
        // for each table column put to output formated data cell
        row['table:table-cell'].push(
          this.getCellWithValue(wbJson.dataCell, value)
        );
      });
      result.push(row);
    });

    return result;
  }

  placeRowsToRoster(rosterObj) {
    const result = [];

    rosterObj.categoryList.forEach((categoryObj) => {
      this.generateRosterHeading(rosterObj, categoryObj, result);

      // export names
      categoryObj.persons.forEach((person) => {
        const row = this.createRow(rosterObj.wbJson.dataRowStyle);

        row['table:table-cell'].push(
          this.getCellWithValue(rosterObj.wbJson.dataCell, {
            value: person.startingNumber,
            type: 'number',
          })
        );
        row['table:table-cell'].push(
          this.getCellWithValue(rosterObj.wbJson.dataCell, {
            value: `${person.lastName} ${person.firstName}`,
            type: 'text',
          })
        );
        categoryObj.disciples.forEach(() => {
          // for each disciple put to output formated data cell
          const cell = this.getCellWithValue(rosterObj.wbJson.dataCell, '');
          // make the cell editable
          cell.$['table:style-name'] = rosterObj.wbJson.editableDataCellStyle;
          row['table:table-cell'].push(cell);
        });
        result.push(row);
      });
    });
    rosterObj.wbJson.contentJson['office:document-content']['office:body'][0][
      'office:spreadsheet'
    ][0]['table:table'][0]['table:table-row'] = result;
    return rosterObj;
  }

  generateRosterHeading(rosterObj, categoryObj, result) {
    // export label
    const { labelRow } = rosterObj.wbJson;
    labelRow['table:table-cell'][0].$['table:number-columns-spanned'] =
      categoryObj.disciples.length + 2;
    // if this is not an Extra category, add "Age From" and "Age To" to roster
    let headerText = categoryObj.categoryName;
    if (!categoryObj.extra) {
      headerText = `${headerText} (${categoryObj.ageTo} - ${categoryObj.ageFrom})`;
    }
    result.push(
      this.getRowWithValue(labelRow, headerText, categoryObj.categoryId)
    );
    const header = this.createRow(rosterObj.wbJson.headerRowStyle);

    // export table level
    header['table:table-cell'].push(
      this.getCellWithValue(
        rosterObj.wbJson.headerCell,
        { value: 'St. číslo', type: 'text' },
        'startingNumber'
      )
    );
    header['table:table-cell'].push(
      this.getCellWithValue(
        rosterObj.wbJson.headerCell,
        { value: 'Jméno', type: 'text' },
        'fullName'
      )
    );
    categoryObj.disciples.forEach((disciple) => {
      header['table:table-cell'].push(
        this.getCellWithValue(
          rosterObj.wbJson.headerCell,
          { value: disciple.label, type: 'text' },
          disciple._id
        )
      );
    });
    result.push(header);
  }

  getTemplateRows(sourceFile) {
    return this.readFile(TEMPLATE_PATH, sourceFile, ODS_CONTENT).then(
      (contentJson) => {
        const rows =
          contentJson['office:document-content']['office:body'][0][
            'office:spreadsheet'
          ][0]['table:table'][0]['table:table-row'];
        const styles =
          contentJson['office:document-content']['office:automatic-styles'][0][
            'style:style'
          ];
        return {
          contentJson,
          labelRow: this.getRowTemplate(rows, '@@LABEL_ROW@@', null),
          headerRowStyle: this.getRowStyle(rows, '@@HEADER_ROW@@'),
          headerCell: this.getCellTemplate(rows, '@@HEADER_ROW@@'),
          dataRowStyle: this.getRowStyle(rows, '@@DATA_ROW@@'),
          dataCell: this.getCellTemplate(rows, '@@DATA_ROW@@'),
          editableDataCellStyle: this.getEditableDataCellStyle(styles),
        };
      }
    );
  }

  getRowStyle(rows, filter) {
    const rowOrNull = rows.find((row) => this.rowContainsText(row, filter));
    if (rowOrNull) return rowOrNull.$['table:style-name'];
    return null;
  }

  getEditableDataCellStyle(styles) {
    const rowOrNull = styles.find(
      (style) =>
        JSON.stringify(style).indexOf('"style:cell-protect":"none"') > 0
    );
    if (rowOrNull) return rowOrNull.$['style:name'];
    return null;
  }

  getCellTemplate(rows, filter) {
    const rowOrNull = rows.find((row) => this.rowContainsText(row, filter));
    if (rowOrNull) return rowOrNull['table:table-cell'][0];
    return null;
  }

  getRowTemplate(list, filter, defaultValue) {
    const filteredList = list.filter((row) =>
      this.rowContainsText(row, filter)
    );
    if (filteredList.length > 0) return filteredList[0];
    return defaultValue;
  }

  getRowWithValue(template, newValue, id) {
    const result = JSON.parse(JSON.stringify(template));
    if (!result['table:table-cell']) result['table:table-cell'] = [];
    if (!result['table:table-cell'][0]['text:p'])
      result['table:table-cell'][0]['text:p'] = [];
    result['table:table-cell'][0]['text:p'][0] = !newValue ? '' : newValue;
    if (id) {
      result['table:table-cell'][0]['office:annotation'] =
        this.createAnnotationContent(id);
    }
    return result;
  }

  getEmptyRow(template) {
    const result = JSON.parse(JSON.stringify(template));
    // delete all cells from row
    result['table:table-cell'] = [];
    return result;
  }

  getCellWithValue(template, newValue, id) {
    const result = template
      ? JSON.parse(JSON.stringify(template))
      : this.createCell('');
    const pureValue =
      newValue && (newValue.value || newValue.value === 0)
        ? `${newValue.value}`
        : '';
    const floatValue = parseFloat(pureValue);
    if (!result['text:p']) result['text:p'] = [];
    if (
      (newValue.type === 'fp' || newValue.type === 'number') &&
      !Number.isNaN(floatValue)
    ) {
      result.$['office:value-type'] = 'float';
      result.$['calcext:value-type'] = 'float';
      result.$['office:value'] = pureValue;
      result['text:p'][0] = pureValue.replace('.', ',');
    } else if (newValue.type === 'time') {
      result.$['office:value-type'] = 'time';
      result.$['calcext:value-type'] = 'time';
      result.$['office:time-value'] = stringService.formatOfficeTime(pureValue);
      result['text:p'][0] = stringService.formatTime(pureValue, false);
    } else {
      result.$['office:value-type'] = 'string';
      result.$['calcext:value-type'] = 'string';
      result['text:p'][0] = pureValue;
    }

    if (id) {
      result['office:annotation'] = this.createAnnotationContent(id);
    }
    return result;
  }

  createRow(rowStyle) {
    return {
      $: {
        'table:style-name': rowStyle,
      },
      'table:table-cell': [],
    };
  }

  createCell(cellStyle) {
    return {
      $: {
        'table:style-name': cellStyle,
      },
    };
  }

  createAnnotationContent(value) {
    return [
      {
        'text:p': [`##${value}##`],
      },
    ];
  }

  rowContainsText(row, text) {
    return JSON.stringify(row).indexOf(`"text:p":["${text}"]`) >= 0;
  }
}

export default new ExcelService();
