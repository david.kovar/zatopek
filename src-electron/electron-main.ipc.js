/**
 * This file specify all IPC communication channels between Renderer and Main process
 */

import { db, DATABASES } from './service/db-access-service';
import categoryService from './service/category-service';
import discipleService from './service/disciple-service';
import discatService from './service/discat-service';
import resultService from './service/result-service';
import resourceService from './service/resource-service';
import settingService from './service/setting-service';
import excelService from './service/excel-service';
import personService from './service/person-service';
import { runAsync } from './service/promise-service';

const { ipcMain } = require('electron');

/** Person model */
ipcMain.on('person-get-all-data', (evt) => {
  runAsync(() => db.findAll(DATABASES.PERSON))
    .then((resp) =>
      evt.sender.send('person-get-all-data-resp', {
        status: 'ok',
        message: resp,
      })
    )
    .catch((err) =>
      evt.sender.send('person-get-all-data-resp', {
        status: 'fail',
        message: err.message,
      })
    );
});
ipcMain.handle('person-filter-data', async (_, selector) => {
  return await runAsync(() => personService.findMatching(selector, true))
    .then((athletes) => {
      return athletes.map((person) => {
        const category = db.getById(
          DATABASES.CATEGORY,
          person.manualCategoryId ? person.manualCategoryId : person.categoryId
        );
        person.categoryName = category ? category.name : '';
        return person;
      });
    })
    .then((resp) => {
        return { status: 'ok', message: resp };
    })
    .catch((err) => {
      return { status: 'fail', message: err.message };
    });
});
ipcMain.handle('person-by-starting-number', async (_, startNumber) => {
  return await runAsync(() => personService.findMatching({ startingNumber: startNumber }, false))
    .then((resp) => ({ status: 'ok', message: resp }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('person-save', async (_, person) => {
  if (!person.sex) person.sex = 'm';

  return await runAsync(() => personService.savePerson(person, true))
    .then(() => { return { status: 'ok' }})
    .catch((err) => {
      console.log(`Error: ${err}`);
      return {
        status: 'fail',
        message: `Error when inserting to DB '${DATABASES.PERSON}': ${err}`,
      };
    });
});
ipcMain.handle('person-delete', async (_, person) => {
  return await runAsync(() => db.delete(person, DATABASES.PERSON))
    .then((resp) => { return { status: 'ok', message: resp }})
    .catch((err) => { return { status: 'fail', message: err.message }});
});

/** Category model */
ipcMain.handle('category-get-all-data', async () => {
  return runAsync(() => db.filter(DATABASES.CATEGORY, {}).docs)
    .then((resp) => {
      if (resp == null || resp.length === 0) {
        return { status: 'ok', message: [categoryService.createDefault()] }
      } else {
        resp = resp.sort((a, b) => a.rank - b.rank);
        return { status: 'ok', message: resp }
      }
    })
    .catch((err) => {
      return { status: 'fail', message: err.message }
    });
});
ipcMain.handle('category-get-all-extra', async () => {
  return await runAsync(() => db.filter(DATABASES.CATEGORY, { extra: { $eq: true } }).docs)
    .then((resp) => {
      if (resp && resp.length > 0) {
        return { status: 'ok', message: resp };
      } else {
        return { status: 'ok', message: []};
      }
    })
    .catch((err) => {
      return { status: 'fail', message: err.message };
    });
});
ipcMain.handle('category-save', async(_, category) => {
  return runAsync(() => categoryService.save(category))
    .then(() => { return {status: 'ok'} })
    .catch((err) => {
      console.log(`Error: ${err}`);
      return {
        status: 'fail',
        message: `Error when inserting to DB '${DATABASES.CATEGORY}': ${err}`,
      };
    });
});
ipcMain.handle('category-delete', async (_, category) => {
  return runAsync(() => categoryService.delete(category))
    .then((resp) => ({ status: 'ok', message: resp } ))
    .catch((err) => ({ status: 'fail', message: err.message } ));
});
ipcMain.handle('category-save-all-ranks', async (_, categories) => {
  return runAsync(() => categoryService.saveAllRanks(categories))
    .then(() => ({ status: 'ok' }))
    .catch((err) => ({
      status: 'fail',
      message: `Error when inserting to DB '${DATABASES.CATEGORY}': ${err}`,
    }));
});

/** Disciple model */
ipcMain.handle('disciple-get-all-data', async () => {
  return runAsync(() => db.findAll(DATABASES.DISCIPLE))
    .then((resp) => ({ status: 'ok', message: resp } ))
    .catch((err) => ({ status: 'fail', message: err.message } ));
});
ipcMain.handle('disciple-filter-data', async (_, categoryId) => {
  return await runAsync(() => discipleService.getDisciplesForCategory(categoryId))
    .then((resp) => ({ status: 'ok', message: resp }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('disciple-save', async (_, disciple) => {
  return runAsync(() => discipleService.save(disciple))
    .then(() => ({ status: 'ok' }))
    .catch((err) => {
      console.log(`Error: ${err}`);
      return {
        status: 'fail',
        message: `Error when inserting to DB '${DATABASES.DISCIPLE}': ${err}`,
      };
    });
});
ipcMain.handle('disciple-delete', async (_, disciple) => {
  return runAsync(() => discipleService.delete(disciple))
    .then((resp) => ({ status: 'ok', message: resp }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});

/** Discat model */
ipcMain.handle('discat-get-all-data', async  () => {
  return runAsync(() => discatService.getAllDiscats())
    .then((disciples) => ({ status: 'ok', message: disciples }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('discat-save', async (_, discats) => {
  return discatService.saveDiscats(discats)
    .then(() => ({ status: 'ok' }))
    .catch((err) => {
      console.log(`Error: ${err}`);
      return {
        status: 'fail',
        message: `Error when inserting to DB '${DATABASES.DISCIPLE}': ${err}`,
      };
    });
});

/** Register */
ipcMain.handle('import-csv', async (_, path) => {
  return await personService
    .importCsv(path)
    .then((exportPath) => {
      return { status: 'ok', exportPath }})
    .catch((err) => {
      return { status: 'fail', message: err.message }});
});

/** Results */
ipcMain.handle('export-category', async (_, categoryId) => {
  return await resultService.exportCategory(categoryId)
    .then((exportPath) => ({ status: 'ok', exportPath }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('export-formatted', async (_, amount) => {
  return await resultService.exportFormatted(amount)
    .then((exportPath) => ({ status: 'ok', exportPath }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('export-plain', async () => {
  return await resultService.exportPlain()
    .then((exportPath) => ({ status: 'ok', exportPath }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.on('print-to-pdf', (evt) => {
  resourceService.printToPdf(() => {
    evt.sender.send('print-to-pdf-resp');
  });
});

/** Settings */
ipcMain.handle('setting-get-all-data', async() => {
  return await runAsync(() => settingService.getSettings())
    .then((resp) => ({  status: 'ok', message: resp }));
});
ipcMain.handle('setting-changed', async (_, setting) => {
  return await runAsync(() => settingService.save(setting)).then((resp) => { return resp; });
});

/** Tables */
ipcMain.handle('export-roster', async (_, categoryList) => {
  return await excelService.exportRoster(categoryList)
      .then((resp) => { return resp; })
      .catch((err) => { console.log(err); })
});
ipcMain.handle('import-results', async(_, odsContent) => {
  return await resultService.importResults(odsContent)
      .then((data) => ({ status: 'ok', message: data }))
      .catch((err) => ({ status: 'fail', message: err.message }));
});
ipcMain.handle('save-imported-results', async (_, results) => {
  return resultService.saveImportedResults(results)
    .then((data) => ({ status: 'ok', message: data }))
    .catch((err) => ({ status: 'fail', message: err.message }));
});
