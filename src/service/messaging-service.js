// outside of a Vue file
import { Notify } from 'quasar';
import {useI18n} from 'vue-i18n';

class MessagingService {
  // static debug(message) {
  //   // TODO console.log(message);
  // }

  static info(message) {
    // TODO console.log(message);
    Notify.create({
      message, color: 'info', icon: 'fas fa-info-circle',
    });
  }

  static warn(message) {
    // TODO console.log(`Warn: ${message}`);
    Notify.create({
      message, color: 'warning', icon: 'fas fa-exclamation',
    });
  }

  static error(message) {
    // TODO console.log(`%cERROR: ${message}`, 'color: red;');
    Notify.create({
      message, color: 'negative', icon: 'fas fa-exclamation-triangle',
    });
  }

  static getI18n() {
    const { t } = useI18n();
    return t;
  }
}

export default MessagingService;
