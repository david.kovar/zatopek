class UxService {
  formatTime(strTime) {
    const numTime = Number(strTime);
    if (!numTime) return '00:00';

    if (numTime > 3600) {
      const hour = Math.floor(numTime / 3600);
      const min = Math.floor((numTime % 3600) / 60);
      const sec = numTime % 60;
      return `${hour}:${this.fillZero(min)}:${this.fillZero(sec)}`;
    }

    const min = Math.floor(numTime / 60);
    const sec = numTime % 60;
    return `${min}:${this.fillZero(sec)}`;
  }

  unformatTime(strTime) {
    let timeParts;
    if (strTime.indexOf(':') > 0) {
      timeParts = strTime.split(':');
    } else if (strTime.indexOf(',') > 0) {
      timeParts = strTime.split(',');
    } else {
      timeParts = [];
      for (let i = strTime.length - 1; i >= 0; i -= 2) {
        const pair = (i > 0 ? `${strTime[i - 1]}${strTime[i]}` : `${strTime[i]}`);
        timeParts.push(pair);
      }
      timeParts = timeParts.reverse();
    }

    let result = 0;
    timeParts.forEach((part) => {
      result *= 60;
      result += Number(part);
    });

    return result;
  }

  fillZero(num) {
    const str = `${num}`;
    if (str.length === 0) return '00';
    if (str.length === 1) return `0${str}`;
    return str;
  }

  // Originally inspired by Trey Huffine
  // (https://levelup.gitconnected.com/debounce-in-javascript-improve-your-applications-performance-5b01855e086)
  //
  // Returns a function, that, as long as it continues to be invoked, will not
  // be triggered. The function will be called after it stops being called for
  // `wait` milliseconds.
  debounce(func, wait) {
    let timeout;

    return function executedFunction(...args) {
      const later = () => {
        clearTimeout(timeout);
        func(...args);
      };

      clearTimeout(timeout);
      timeout = setTimeout(later, wait);
    };
  }
}

export default new UxService();
