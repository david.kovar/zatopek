class Setting {
  constructor(id, exportPath) {
    this.exportPath = exportPath || '.';
  }
}

export default Setting;
