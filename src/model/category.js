const _SEX_ENUM = Object.freeze({ MALE: 'm', FEMALE: 'f', BOTH: 'b' });

class Category {
  constructor(name, ageTo, sex, rank) {
    this._id = null;
    this.name = name;
    this.ageTo = ageTo;
    this.sex = sex;
    this.rank = rank;
    // if true this category won't be attached automatically but must be set manually
    this.extra = false;
  }
}

export default Category;
export const SEX_ENUM = _SEX_ENUM;
