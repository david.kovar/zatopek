/**
 * Cross-table binding Categories and Disciples
 */
class Discat {
  constructor(categoryId) {
    this.categoryId = categoryId;
  }
}

export default Discat;
