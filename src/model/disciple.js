const _DIRECTION_ENUM = Object.freeze({ ASCENDING: 'a', DESCENDING: 'd' });
// TODO floating point number
const _DATATYPE_ENUM = Object.freeze({ NUMBER: 'number', TIME: 'time' });

class Disciple {
  constructor(categoryId, colName, label, datatype, direction) {
    this._id = null;
    this.colName = colName;
    this.label = label;
    this.datatype = datatype;
    this.direction = direction;
  }
}

export default Disciple;
export const DIRECTION_ENUM = _DIRECTION_ENUM;
export const DATATYPE_ENUM = _DATATYPE_ENUM;
