class Person {
  constructor(
    startingNumber, firstName, lastName, year, sex, email, club,
    manualCategoryId, resigned,
  ) {
    this._id = null;
    this.startingNumber = startingNumber;
    this.firstName = firstName;
    this.lastName = lastName;
    this.year = year;
    this.sex = sex;
    this.email = email;
    this.club = club;
    this.manualCategoryId = manualCategoryId;
    this.resigned = resigned;
    this._entity = null;
  }
}

export default Person;
