export default {
  failed: 'Akce selhala',
  success: 'Akce proběhla úspěšně',
  default: {
    register: 'Registrace',
    tables: 'Tabulky',
    results: 'Výsledky',
    settings: 'Nastavení',
    male: 'Muž',
    female: 'Žena',
    save: 'Uložit',
    cancel: 'Zrušit',
    sexBoth: 'Všichni',
    areYouSure: 'Jseš si jist?',
    categoryFilter: 'Filtr kategorií',
  },

  register: {
    startingNumber: 'Start. č.',
    firstName: 'Jméno',
    lastName: 'Příjmení',
    year: 'Rok nar.',
    email: 'Email',
    club: 'Klub / Město',
    resigned: 'Rezignoval',
    manual_category: 'Kategorie (manuálně)',
    deletePrompt: 'Opravdu chceš smazat {firstName} {lastName} (#{id})?',
    deleteSuccess: 'Uživatel byl úspěšně smazán',
    saveSuccess: 'Uživatel byl úspěšně uložen',
    fillCategorySuccess: 'Kategorie byly úspěšně přiřazeny ke všem sportovcům',
    duplicateStartingNumber:
      'Startovní číslo {number} již je registrované. Chcete ho registrovat znovu?',
    import: {
      label: 'Import z CSV',
      description:
        '<p>Importuje sportovce do registrace z CSV souboru. ' +
        'Oddělovačem musí být středník (;), text může být uzavřen v uvozovkách ("), tabulka je bez záhlaví</p>' +
        'Sloupce musí být v tomto pořadí: <ul><li>Startovní číslo</li><li>Příjmení</li><li>Jméno</li><li>Rok narození</li>' +
        '<li>Pohlaví (M = muž/F = žena)</li><li>Email</li><li>Tým/Město</li><li>Kategorie (volitelně)</li></ul>',
      warning:
        'Pokud sportovce s importovaným startovním číslem již existuje, bude přepsán',
      ok: 'Importovat',
      cancel: 'Zrušit',
      success: 'CSV soubor byl úspěšně naimportován',
      error: 'Chyba při importu dat. Rádek {row}, Sloupec "{col}": {error}',
    },
  },

  tables: {
    results: {
      h1: 'Tabulka výkonů',
    },
    importResults: {
      heading: 'Importuj výsledky',
      dragHere: 'Přetáhni vyplněnou soupisku'
    },
    exportRoster: 'Exportovat soupisku',
    exportSuccessfull: 'Tabulka úspěšně vyexportována do složky {exportPath}',
    saveToCsv: 'Ulož do CSV',
    saveToOds: 'Exportuj do ODS',
    saveToOdsMessage: 'Vyberte prosím kategorie, které chcete exportovat',
    searchById: 'Vyhledej podle startovního čísla',
    personIsResigned:
      '{lastName} {firstName} rezignoval(a). Nelze pro ni vyplnit čas',
  },

  results: {
    outcomes: 'Výstup',
    exportCategory: 'Exportuj tuto kategorii',
    exportCategorySuccess:
      'Výsledky pro kategorii {category} úspěšně vyexportována do složky {exportPath}',
    exportFormatted: 'Exportuj formátované výsledky',
    exportFormattedSuccess:
      'Formátované výsledky úspěšně vyexportovány do složky {exportPath}',
    exportPlain: 'Exportuj vše',
    exportPlainSuccess:
      'Celkové výsledky úspěšně vyexportovány do složky {exportPath}',
    export: {
      message:
        'Vyexportuje výsledky pro všechny kategorie do jednoho souboru. ' +
        'Můžete si vybrat, jestli v každé kategorii vyexportuje všechny sportovce, nebo jen ty na stupních vítězů',
      all: 'Všechny',
      best: 'Stupně vítězů',
    },
  },

  settings: {
    emptyDirWarn:
      'Z důvodu omezení v Javascriptu není možné otevřít prázdnou složku. Napište prosím cestu do řádku ručně',
    categories: {
      h1: 'Kategorie',
      name: 'Název',
      ageTo: 'Rok nar. do (včetně)',
      sex: 'Pohlaví',
      extra: 'Speciální',
      deletePrompt: 'Opravdu chceš smazat kategorii "{category}"',
      mandatoryCheck: 'Jméno a Rok narození jsou povinné položky',
      saveSuccess: 'Kategorie byla úspěšně uložen',
      deleteSuccess: 'Kategorie byla úspěšně smazaná',
      reorderSuccess: 'Kategorie byly úspěšně uloženy',
    },

    disciples: {
      h1: 'Disciplíny',
      bestTop: 'Nejvyšší hodnota',
      bestBottom: 'Nejnižší hodnota',
      typeNumber: 'Celé číslo',
      typeFp: 'Desetinné číslo',
      typeTime: 'Čas',
      category: 'Kategorie',
      name: 'Název',
      datatype: 'Typ hodnot',
      direction: 'Nejlepší je',
      mandatoryCheck: 'Název je povinná položka',
      deletePrompt:
        'Opravdu chceš smazat disciplínu "{disciple}" pro kategorii "{category}"',
      deleteSuccess: 'Disciplína úspěšně smazaná',
    },

    discat: {
      h1: 'Přiřazení disciplín ke kategoriím',
      saveSuccess: 'Nastavení bylo úspěšně uloženo',
    },

    reorder: {
      h1: 'Změna pořadí',
      hint: 'Přetažením změňte pořadí kategorií, potvrďte tlačítkem OK',
    },

    output: {
      h1: 'Výstupy',
      exportPath: 'Exportovat do složky',
    },
  },
};
