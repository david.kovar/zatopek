export default {
  failed: 'Action failed',
  success: 'Action was successful',
  default: {
    register: 'Registration',
    tables: 'Tables',
    results: 'Results',
    settings: 'Settings',
    male: 'Male',
    female: 'Female',
    sexBoth: 'All',
    areYouSure: 'Are you sure?',
    categoryFilter: 'Category filter',
  },

  register: {
    startingNumber: 'Start #',
    firstName: 'First Name',
    lastName: 'Last Name',
    year: 'Year',
    email: 'Email',
    club: 'Club / Town',
    resigned: 'Resigned',
    manual_category: 'Category (manually)',
    deletePrompt: 'Are you sure to delete {firstName} {lastName} (#{id})?',
    deleteSuccess: 'User sucessfuly deleted',
    saveSuccess: 'User sucessfuly saved',
    fillCategorySuccess:
      'Categories successfully filled to registered sportsmen',
    duplicateStartingNumber:
      'Starting number {number} is already registered. Are you sure to register it again?',
    import: {
      label: 'Import from CSV',
      description:
        '<p>Imports athletes to registration from CSV file. ' +
        'Columns must be delimited by semicolon (;), text fields may be enclosed by quotest ("), table is without heading</p>' +
        'Columns must be in this order: <ul><li>Starting number</li><li>Surname</li><li>First name</li>' +
        '<li>Birth year</li><li>Sex (M/F)</li><li>Email</li><li>Team/Town</li><li>Category (optional)</li></ul>',
      warning:
        'If imported starting number already exists, ' +
        'corresponding athlete (with this starting number) will be overwritten',
      ok: 'Start import',
      cancel: 'Cancel',
      success: 'CSV file was successfuly imported',
      error: 'Error in imported file. Row {row}, column "{col}": {error}',
    },
  },

  tables: {
    results: {
      h1: 'Record tables',
    },
    exportSuccessfull: 'Tables were successfully exported to path {exportPath}',
    saveToCsv: 'Save to CSV',
    searchById: 'Search by starting number',
    exportRoster: 'Export roster',
    importResults: {
      heading: 'Import filled roster',
      dragHere: 'Drag ods file here',
      success: 'Results were successfuly imported',
      checkHeading: 'Please check imported values',
    },
    saveToOdsMessage: 'Please select categories to be exported',
    personIsResigned: "{lastName} {firstName} resigned. You can't fill results",
  },

  results: {
    outcomes: 'Outcomes',
    exportCategory: 'Export category',
    exportCategorySuccess:
      'Results were successfully exported for category {category} to path {exportPath}',
    exportFormatted: 'Export formated results',
    exportFormattedSuccess:
      'Formated results were successfully exported to path {exportPath}',
    exportPlain: 'Export all',
    exportPlainSuccess:
      'Plain results were successfully exported to path {exportPath}',
    export: {
      message:
        'Export results for all categories. ' +
        'You can choose, whether all athletes shoud be exported for category, or only those on podium',
      all: 'All atheletes',
      best: 'On podium only',
    },
    printToPdf: 'Print to PDF',
  },

  settings: {
    emptyDirWarn:
      'Due to limitations in Javascript it is not possible to select empty folder. Please write full path directly to edit box',
    categories: {
      h1: 'Categories',
      name: 'Name',
      ageTo: 'Birth year to (including)',
      sex: 'Sex',
      extra: 'Extra',
      deletePrompt: 'Are you sure to delete category "{category}"',
      mandatoryCheck: 'Name and Birth year are mandatory',
      deleteSuccess: 'Category sucessfuly deleted',
      reorderSuccess: 'Categories have been successfuly saved',
    },

    disciples: {
      h1: 'Disciples',
      bestTop: 'Highest value',
      bestBottom: 'Lowest value',
      typeNumber: 'Number',
      typeFp: 'Floating point number',
      typeTime: 'Time',
      name: 'Name',
      datatype: 'Type',
      direction: 'Best is',
      mandatoryCheck: 'Name is mandatory',
      deletePrompt:
        'Are you sure to delete disciple "{disciple}" for category "{category}"',
      deleteSuccess: 'Disciple sucessfuly deleted',
      saveSuccess: 'Disciple sucessfuly saved',
    },

    discat: {
      h1: 'Attach disciples to categories',
      saveSuccess: 'Settings was successfuly saved',
    },

    output: {
      h1: 'Output',
      exportPath: 'Export to folder',
    },
  },
};
